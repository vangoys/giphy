
import UIKit

class SearchTermCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var textLabel: UILabel!
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        textLabel.text =  nil
    }
}
