
import UIKit

class GifDetailViewController: UIViewController {
    @IBOutlet weak var imageView: UIImageView!
    
    weak var gif: UIImage? = UIImage()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if let gif = gif {
            self.imageView.frame.size = gif.size
            self.imageView.image = gif
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
}
