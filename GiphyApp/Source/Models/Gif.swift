
import Foundation

struct Gif {
    let url: URL
    let title: String
}
